//
//  Utils.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 19.09.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

class Utils {
    enum LogType {
        case info, warn, error
    }
    
    enum ErrorCode: String {
        case noUser = "noUser", keyCheckFailed = "keyCheckFailed", firebaseError = "firebaseError", parseError = "parseError"
    }
    
    static func log(logType type: LogType, text: String, logLocation location: String?, code: ErrorCode?, error: Error?) {
        switch type {
        case .info:
            print("[INFO] - \(location ?? "<not specified>"): \(text)")
        case .warn:
            print("[WARN] - \(location ?? "<not specified>"): \(text)")
        case .error:
            print("[ERROR] - \(location ?? "<not specified>"): \(text); Code: \(code?.rawValue ?? "<not specified>"); Error: \(error?.localizedDescription ?? "<not specified>")")
        }
    }
}
