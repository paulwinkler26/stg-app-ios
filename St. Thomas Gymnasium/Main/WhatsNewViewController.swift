//
//  WhatsNewViewController.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 18.09.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit

class WhatsNewViewController: UIViewController {

    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var textOne: UILabel!
    @IBOutlet weak var textTwo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    func setupUI() {
        let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        versionLabel.text = "Version \(currentVersion)"
        
        if #available(iOS 13.0, *) {
            // alright
        } else {
            // setup manually manually
            for text in [textOne, textTwo] {
                text?.font = text?.font.withSize(16)
            }
        }
    }

    @IBAction func gotItPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
