//
//  MainViewController.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 03.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class MainViewController: UITabBarController, UITabBarControllerDelegate{

    // MARK: Lets and Vars
    var currentTabIndex = 0

    // MARK: Outlets
    @IBOutlet weak var theTabBar: UITabBar!

    // MARK: App life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        delegate = self
        createMotherVars()
        checkVersion()
        
        if #available(iOS 13.0, *) {
            // alright
        } else {
            // setup manually manually
            setTabBarIcons()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loginSegue" {
            let destination = segue.destination as! LoginViewController
            destination.parentVC = self
        }
    }

    // Stuff for opening the popup when the third element is clicked
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        currentTabIndex = self.selectedIndex
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if self.selectedIndex == 2 {
            self.selectedIndex = currentTabIndex
            showTimeTables()
        }
    }
    
    func checkVersion() {
        let safedVersion = UserDefaults.standard.string(forKey: "version")
        let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
        
        if safedVersion != nil && safedVersion == currentVersion {
            Utils.log(logType: .info, text: "Correct Version was found. Version: \(currentVersion)", logLocation: "MainViewVC", code: nil, error: nil)
        } else {
            Utils.log(logType: .info, text: "New Version was downloaded. New Version: \(currentVersion); Old Version: \(safedVersion ?? "nil")", logLocation: "MainViewVC", code: nil, error: nil)
            UserDefaults.standard.setValue(currentVersion, forKey: "version")
           
            DispatchQueue.main.async {
                if STGAuth.checkIfUserIsLoggedIn() {
                    self.updateUserData()
                }
                
                self.performSegue(withIdentifier: "newVersionSegue", sender: nil)
            }
        }
    }
    
    func updateUserData() {
        STGMessaging.unsubscribe(from: UserDefaults.standard.string(forKey: "klasse")!)
        
        STGFirestore.getUserData(onSuccess: { (data) in
            for (key, value) in data {
                UserDefaults.standard.set(value, forKey: key)
            }
            
            Analytics.logEvent("UserDataUpdate", parameters: nil)
            
            STGMessaging.subscribe(to: data["klasse"]!)
            
            if data["group"]!.elementsEqual("Admin") {
                STGMessaging.subscribe(to: "admin")
            }
            
            Utils.log(logType: .info, text: "Userdata was updated", logLocation: "MainViewVC", code: nil, error: nil)
        }) { (err) in
            UserDefaults.standard.setValue("error", forKey: "version")
            Utils.log(logType: .error, text: "Could not update userdata", logLocation: "MainViewVC", code: .firebaseError, error: err)
        }
    }
    
    func showTimeTables() {
        performSegue(withIdentifier: "timeTableSegue", sender: nil)
    }

    // Setups the bar icons manually because iOS < 13 doesn't have predefined icons
    func setTabBarIcons() {
        theTabBar.items?[0].image = UIImage(named: "newsIcon")
        theTabBar.items?[1].image = UIImage(named: "vertretungsIcon")
        theTabBar.items?[2].image = UIImage(named: "planIcon")
        theTabBar.items?[3].image = UIImage(named: "kontaktIcon")
        theTabBar.items?[4].image = UIImage(named: "settingsIcon")
    }

    // Initialize the mother/parent values in the different childviewcontroller
    func createMotherVars() {
        let homeVC = self.children[0] as! HomeViewController
        homeVC.mother = self
        
        
        let substituteVC = self.children[1] as! SubstituteViewController
        substituteVC.mother = self
        
        let settingsVC = self.children[4] as! SettingsViewController
        settingsVC.mother = self
    }

    // Opens login segue
    func performLogin() {
        self.performSegue(withIdentifier: "loginSegue", sender: nil)
    }

    // Reloads all data in all necessary view controllers because of an account action e.g.
    func reloadAllData() {
        let homeVC = self.children[0] as! HomeViewController
        let substituteVC = self.children[1] as! SubstituteViewController
        let settingsVC = self.children[4] as! SettingsViewController
        
        homeVC.data.messages.removeAll()
        homeVC.data.news.removeAll()
        homeVC.downloadNews()
        
        substituteVC.data.clear()
        substituteVC.substituteTableView?.reloadData()
        substituteVC.downloadSubstitutes()
        
        settingsVC.setupData()
    }
}
