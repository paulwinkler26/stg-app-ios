//
//  LoginViewController.swift
//  St. Thomas Gymnasium
//
//  Created by Paul Winkler on 17.07.19.
//  Copyright © 2019 STG-Devs. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import JGProgressHUD

class LoginViewController: UIViewController {
    
    // MARK: - Lets and Vars
    let userData = UserDefaults.standard
    var username = ""
    var password = ""
    var parentVC: MainViewController?
    
    // MARK: - Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var borderOne: UIView!
    @IBOutlet weak var usernameView: UIView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var borderTwo: UIView!
    @IBOutlet weak var goOnButton: UIButton!
    @IBOutlet weak var rightsInfoLabel: UILabel!
    @IBOutlet weak var seeRightsButton: UIButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var backButton: UIButton!
    
    // MARK: - App Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        setupUI()

        // Back button for iOS < 13
        if #available(iOS 13.0, *) {
            backView.isHidden = true
        } else {
            backView.layer.cornerRadius = backView.frame.width / 2
            backButton.layer.cornerRadius = backButton.frame.width / 2
            backView.addShadowWithRGB(ra: 5, wx: 0, wy: 3, r: 0, g: 0, b: 0, a: 0.8)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    // MARK: - Actions
    @IBAction func goOnKeyPressed(_ sender: UITextField) {
        passwordTextField.becomeFirstResponder()
    }
    
    @IBAction func goOnButtenPressed(_ sender: UIButton) {
        username = usernameTextField.text!
        password = passwordTextField.text!
        
        view.endEditing(true)
        
        performLogin()
    }
    
    @IBAction func finishKeyPressed(_ sender: UITextField) {
        username = usernameTextField.text!
        password = passwordTextField.text!
        
        view.endEditing(true)
        
        performLogin()
    }
    
    @IBAction func seeRightsButtonPressed(_ sender: UIButton) {
        guard let url = URL(string: "http://stgapp.de/hilfe/rechtliches/datenschutz") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func backButtonPressed() { dismiss(animated: true, completion: nil) }
    
    // MARK: - UI Design
    func setupUI() {
        // Main View
        mainView.layer.cornerRadius = 20
        mainView.addPrecastShadow()

        // Logo Image View
        logoImageView.layer.cornerRadius = 22
        
        // Border One
        borderOne.layer.cornerRadius = 2
        borderOne.backgroundColor = UIColor(red: 112/255, green: 112/255, blue: 122/255, alpha: 1)
        
        usernameTextField.attributedPlaceholder = NSAttributedString(string: "Nutzername", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 5/255, green: 5/255, blue: 5/255, alpha: 0.4)])
        
        // Password Text Field
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Passwort", attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 5/255, green: 5/255, blue: 5/255, alpha: 0.4)])
        
        // Border Two
        borderTwo.layer.cornerRadius = 2
        borderTwo.backgroundColor = UIColor(red: 112/255, green: 112/255, blue: 122/255, alpha: 1)
        
        // Go On Button
        goOnButton.backgroundColor = UIColor(named: "DateViewActive")
        goOnButton.tintColor = UIColor.white

        [usernameView, passwordView, goOnButton].forEach { element in
            element?.layer.cornerRadius = 23
            element?.addPrecastShadow()
        }
    }
    
    // MARK: - Firebase and Data Handling
    func performLogin() {
        let hud = JGProgressHUD(style: .dark)
        
        // Return, if information is missing
        if username == "" || password == "" {
            hud.show(in: self.view)
            showError(content: "Eingabe Unvollständig", hud: hud)
            return
        }
        
        // Remove space at the end of the username
        let lastCharakter = username.suffix(1)
        
        if lastCharakter == " " {
            username = String(username.prefix(username.count - 1))
        }
        
        // change username to email
        let email = "\(username)@thomas-gymnasium.de"
        
        // Show Loading
        showLoading(hud: hud)
        
        STGAuth.logIn(email: email, password: password, onSuccess: { (result) in
            Utils.log(logType: .info, text: "Successfully logged in", logLocation: "LoginVC", code: nil, error: nil)
            self.showSuccess(content: "Erfolgreich", hud: hud)
            self.handleUserData(hud: hud)
        }) { (err) in
            switch err.localizedDescription {
            case "": self.showError(content: "Einloggen Fehlgeschlagen", hud: hud)
            default:
                self.showError(content: "Einloggen Fehlgeschlagen - \(err.localizedDescription)", hud: hud)
            }
        }
    }

    func handleUserData(hud: JGProgressHUD) { // Download User Data
        STGFirestore.getUserData(onSuccess: { (data) in
            for (key, value) in data {
                self.userData.set(value, forKey: key)
                
                self.userData.set(true, forKey: "googleAnalytics") // Einstellungen
                self.userData.set(1, forKey: "theme")
            }
            
            Analytics.logEvent(AnalyticsEventLogin, parameters: [AnalyticsParameterMethod: self.performLogin])
            
            STGMessaging.subscribe(to: data["klasse"]!)
            
            if data["group"]!.elementsEqual("Admin") {
                STGMessaging.subscribe(to: "admin")
            }
            
            Utils.log(logType: .info, text: "Successfully fetched and saved userdata", logLocation: "LoginVC", code: nil, error: nil)
            
            self.parentVC!.reloadAllData()
            
            // Perform the sgue
            Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { timer in
                self.dismiss(animated: true) {}
            }
        }) { (err) in
            Utils.log(logType: .error, text: "Could not fetch userdata from firebase", logLocation: "LoginVC", code: .firebaseError, error: err)
            self.showError(content: "Fehler bei der Nutzerkonfiguration, bitte kontaktiere einen Lehrer", hud: hud)
            return
        }
    }

    // Helper function
    func showLoading(hud: JGProgressHUD) { // Hud-Prozess zulang => funktion erstellt
        hud.textLabel.text = "Loading"
        hud.show(in: self.view)
    }
    
    func showError(content: String, hud: JGProgressHUD) { // Hud-Prozess zulang => funktion erstellt
        hud.textLabel.text = content
        hud.indicatorView = JGProgressHUDErrorIndicatorView()
        hud.dismiss(afterDelay: 2)
    }
    
    func showSuccess(content: String, hud: JGProgressHUD) { // Hud-Prozess zulang => funktion erstellt
        hud.textLabel.text = content
        hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        hud.dismiss(afterDelay: 2)
    }
}
