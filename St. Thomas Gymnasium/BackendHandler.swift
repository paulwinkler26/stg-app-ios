//
//  BackendHandler.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 09.09.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import FirebaseAuth
import FirebaseFirestore
import FirebaseMessaging
import FirebaseStorage

let auth = Auth.auth()
let firestore = Firestore.firestore()
let storage = Storage.storage()
let messaging = Messaging.messaging()

class STGAuth {
    static func logIn(email: String, password: String, onSuccess: @escaping (_ result: AuthDataResult) -> Void, onError: @escaping (_ error: Error) -> Void) {
        auth.signIn(withEmail: email, password: password) { (result, error) in
            if let err = error {
                onError(err)
            } else {
                onSuccess(result!)
            }
        }
    }
    
    static func getUID() -> String {
        if let user = auth.currentUser {
            return user.uid
        } else {
            return ""
        }
    }
    
    static func checkIfUserIsLoggedIn() -> Bool {
        return auth.currentUser != nil
    }
    
    static func logOut() -> Bool {
        do {
            try auth.signOut()
            return true
        } catch {
            return false
        }
    }
    
    static func changePassword(password: String, newPassword: String, onSuccess: @escaping () -> Void, onError: @escaping (_ error: Error?) -> Void) {
        guard let user = auth.currentUser else {
            onError(nil)
            return
        }
        
        let credential: AuthCredential = EmailAuthProvider.credential(withEmail: user.email!, password: password)
        
        user.reauthenticate(with: credential, completion: { (result, error) in
            if let err = error {
                onError(err)
            } else {
                user.updatePassword(to: newPassword) { (error2) in
                    if let err = error2 {
                        onError(err)
                    } else {
                        onSuccess()
                    }
                }
            }
        })
    }
    
    static func resetPassword(email: String, onSuccess: @escaping () -> Void, onError: @escaping (_ error: Error?) -> Void) {
        auth.sendPasswordReset(withEmail: email) { (error) in
            if let err = error {
                Utils.log(logType: .error, text: "Could not reset password", logLocation: "SettingsViewController", code: .firebaseError, error: err)
                onError(err)
            } else {
                onSuccess()
            }
        }
    }
}

class STGFirestore {
    static func getUserData(onSuccess: @escaping (_ userData: Dictionary<String, String>) -> Void, onError: @escaping (_ error: Error) -> Void) {
        firestore.collection("users").document(STGAuth.getUID()).getDocument { (snapshot, error) in
            if let err = error {
                onError(err)
            } else {
                guard snapshot?.get("vorname") != nil else {
                    return
                }

                let user: [String: String] = [
                    "name": "\(snapshot?.get("vorname") ?? "") \(snapshot?.get("nachname") ?? "")",
                    "group": snapshot?.get("group") as! String,
                    "uid": STGAuth.getUID(),
                    "klasse": snapshot?.get("klasse") as! String
                ]

                onSuccess(user)
            }
        }
    }
    
    static func getNews(onSuccess: @escaping (_ news: [News]) -> Void, onError: @escaping (_ error: Error) -> Void) {
        firestore.collection("neuigkeiten").getDocuments { (snapshot, error) in
            if let err = error {
                onError(err)
            } else {
                var news = [News]()
                
                for document in snapshot!.documents {
                    let data = document.data()
                    
                    guard data.keys.count == 5 else {
                        Utils.log(logType: .error, text: "News download failed", logLocation: "HomeViewController", code: .keyCheckFailed, error: nil)
                        continue
                    }
                    
                    news.append(
                        News(
                            bild: data["bild"] as! String,
                            bildTitel: data["bildTitel"] as! String,
                            name: data["name"] as! String,
                            text: (data["text"] as! String).replacingOccurrences(of: "-nl", with: "\n"),
                            titel: data["titel"] as! String,
                            id: document.documentID
                        )
                    )
                }
                
                onSuccess(news)
            }
        }
    }
    
    static func getMessages(onSuccess: @escaping (_ messages: [Message]) -> Void, onError: @escaping (_ error: Error) -> Void) {
        guard let klasse = UserDefaults.standard.string(forKey: "klasse") else {
            Utils.log(logType: .warn, text: "No class found", logLocation: "HomeViewController", code: nil, error: nil)
            return
        }
        
        firestore.collection("nachrichten").whereField("klasse", arrayContains: klasse).order(by: "datum").getDocuments { (snapshot, error) in
            if let err = error {
                onError(err)
            } else {
                var messages = [Message]()
                
                for document in snapshot!.documents {
                    let data = document.data()
                    
                    guard data.keys.count == 5 else {
                        Utils.log(logType: .error, text: "The data of message \(document.documentID) are incomplete", logLocation: "HomeViewController", code: .keyCheckFailed, error: nil)
                        continue
                    }
                    
                    messages.append(Message(
                        datum: data["datum"] as! String,
                        klasse: [klasse], // Die Korrekte Klasse ist hier nicht eingebunden, später beheben!
                        name: data["name"] as! String,
                        text: data["text"] as! String,
                        uid: data["uid"] as! String
                    ))
                }
                
                onSuccess(messages)
            }
        }
    }
    
    static func getSubstitutes(userIsTeacher teacher: Bool, klasse: String?, onSuccess: @escaping (_ messages: [[Substitute]]) -> Void) {
        let dates = [
            Date().shortString(fromDate: Date()).replacingOccurrences(of: ".", with: "-"),
            Date().shortString(fromDate: Date().dayAfter).replacingOccurrences(of: ".", with: "-"),
            Date().shortString(fromDate: Date().dayAfterTomorrow).replacingOccurrences(of: ".", with: "-")
        ]
        
        let getRef = { (i: Int) -> Query in
            if teacher {
                return firestore.collection("vertretungen").whereField("datum", isEqualTo: dates[i]).order(by: "stunde")
            } else {
                return firestore.collection("vertretungen").whereField("klasse", arrayContains: klasse!).whereField("datum", isEqualTo: dates[i]).order(by: "stunde")
            }
        }
        
        var substituteData = [[Substitute](), [Substitute](), [Substitute]()]
        var t = 0
        for i in 0..<3 {
            getRef(i).getDocuments { (snapshot, error) in
                if let err = error {
                    Utils.log(logType: .error, text: "Error while downloading substitutes", logLocation: "SubstituteViewController", code: .firebaseError, error: err)
                } else {
                    for document in snapshot!.documents {
                        let data = document.data()
                        
                        if data.keys.count == 7 {
                            substituteData[i].append(Substitute(icon: data["icon"] as! String, stunde: data["stunde"] as! String, name: data["name"] as! String, datum: data["datum"] as! String))
                        }
                    }
                }
                
                t += 1
                if (t == 3) {
                    onSuccess(substituteData)
                }
            }
        }
    }
    
    static func getMealPlan(onSuccess: @escaping (_ messages: [Meal]) -> Void, onError: @escaping (_ error: Error) -> Void) {
        let weekOfYear = Calendar.current.component(.weekOfYear, from: Date.init(timeIntervalSinceNow: 0))
        let week = "\(weekOfYear < 10 ? "0" : "")\(weekOfYear)"
        Utils.log(logType: .info, text: "Getting Week for Meal Plan; Week \(week)", logLocation: nil, code: nil, error: nil)
        
        firestore.collection("speiseplan").document(week).getDocument { (snapshot, error) in
            if let err = error {
                Utils.log(logType: .error, text: "Error while downloading substitutes", logLocation: "TimeTableViewController", code: .firebaseError, error: err)
                onError(err)
            } else {
                guard let data = snapshot?.data(), data.count != 0 else {
                    Utils.log(logType: .warn, text: "No data yet", logLocation: "TimeTable", code: nil, error: nil)
                    return
                }
                
                var mealPlan = [Meal]()
                
                for day in ["montag", "dienstag", "mittwoch", "donnerstag"] {
                    if let dayData = data[day] as? [String: String] {
                        guard dayData.keys.count == 4 else {
                            Utils.log(logType: .error, text: "Not enugh meal key", logLocation: "TimeTableViewController", code: .keyCheckFailed, error: nil)
                            continue
                        }
                        
                        mealPlan.append(Meal(
                            vorspeise: dayData["vorspeise"] ?? "",
                            hauptspeise: dayData["hauptspeise"] ?? "",
                            nachspeise: dayData["nachspeise"] ?? "",
                            vegetarisch: dayData["vegetarisch"] ?? "" == "" ? "gleich Hauptspeise": dayData["vegetarisch"] ?? "",
                            date: dayData["datum"] ?? "",
                            day: day.capitalizingFirstLetter(),
                            type: dayData["vorspeise"] ?? "" != "" && dayData["nachspeise"] ?? "" == "" ? .type1 : dayData["vorspeise"] ?? "" == "" && dayData["nachspeise"] ?? "" != "" ? .type2 : .type3
                        ))
                    } else {
                        Utils.log(logType: .info, text: "NO NO NO", logLocation: nil, code: nil, error: nil)
                    }
                }
                
                print("Meal Plan: \(mealPlan)")
                
                onSuccess(mealPlan)
            }
        }
    }
    
    static func getTimeTableV2(onSuccess: @escaping (_ messages: [String: [String: [[String: String?]]]]?) -> Void, onError: @escaping (_ error: Error) -> Void) {
        guard let klasse = UserDefaults.standard.string(forKey: "klasse") else {
            Utils.log(logType: .error, text: "No class was found", logLocation: "TimeTableViewController", code: .noUser, error: nil)
            return
        }
        
        firestore.collection("stundenplanV2").document(klasse).getDocument { (snapshot, error) in
            if let err = error {
                Utils.log(logType: .error, text: "Error while downloading substitutes", logLocation: "TimeTableViewController", code: .firebaseError, error: err)
                onError(err)
                return
            } else if let data = snapshot?.data()?["plan"] as? [String: [String: [[String: String?]]]] {
                if data.isEmpty {
                    onSuccess(nil)
                    return
                }
                
                onSuccess(data)
                return
            }
            
            Utils.log(logType: .error, text: "Recieved invalid data from firebase", logLocation: "TimeTableViewController", code: .firebaseError, error: nil)
            onSuccess(nil)
        }
    }
    
    static func downloadOwnMessages(userIsAdmin admin: Bool, onSuccess: @escaping (_ messages: [MyMessage]) -> Void) {
        let ref = admin ? firestore.collection("nachrichten") : firestore.collection("nachrichten").whereField("uid", isEqualTo: STGAuth.getUID())
        
        ref.getDocuments { (snapshot, error) in
            if let err = error {
                Utils.log(logType: .error, text: "Error while downloading my message", logLocation: "MyMessagesVC", code: .firebaseError, error: err)
            } else {
                var messages = [MyMessage]()
                
                for document in snapshot!.documents {
                    let data = document.data()
                    
                    for attr in ["datum", "uid", "text"] {
                        guard let _ = data[attr] as? String else {
                            Utils.log(logType: .error, text: "Not enugh keys", logLocation: "MyMessagesVC", code: .keyCheckFailed, error: nil)
                            return
                        }
                    }
                    
                    messages.append(MyMessage(text: data["text"] as! String, documentID: document.documentID))
                }
                
                onSuccess(messages)
            }
        }
    }
    
    static func deleteOwnMessage(id: String, onSuccess: @escaping () -> Void, onError: @escaping () -> Void) {
        firestore.collection("nachrichten").document(id).delete { (error) in
            if let err = error {
                Utils.log(logType: .error, text: "Could not delete own message with id \(id)", logLocation: "MyMessagesVC", code: .firebaseError, error: err)
            } else {
                onSuccess()
            }
        }
    }
    
    static func sendMessage(messageData: [String: Any], onSuccess: @escaping () -> Void, onError: @escaping (Error) -> Void) {
        firestore.collection("nachrichten").addDocument(data: messageData) { (error) in
            if let err = error {
                Utils.log(logType: .error, text: "Failed to send message", logLocation: "MyMessagesVC", code: .firebaseError, error: err)
                onError(err)
            } else {
                onSuccess()
            }
        }
    }
}

class STGMessaging {
    static func subscribe(to topic: String) {
        messaging.subscribe(toTopic: topic) { error in
            if let err = error {
                Utils.log(logType: .error, text: "Couldn't subscribe to topic \(topic)", logLocation: "Messaging", code: .firebaseError, error: err)
            } else {
                Utils.log(logType: .info, text: "Subscribed to topic \(topic)", logLocation: "Messaging", code: nil, error: nil)
            }
        }
    }
    
    static func unsubscribe(from topic: String) {
        messaging.unsubscribe(fromTopic: topic) { (error) in
            if let err = error {
                Utils.log(logType: .error, text: "Couldn't unsubscribe to topic \(topic)", logLocation: "Messaging", code: .firebaseError, error: err)
            } else {
                Utils.log(logType: .info, text: "Unsubscribed to topic \(topic)", logLocation: "Messaging", code: nil, error: nil)
            }
        }
    }
}

class STGStorage {
    static func getImage(path: String, onSuccess: @escaping (_ data: Data) -> Void, onError: @escaping (_ error: Error) -> Void) {
        storage.reference(withPath: path).getData(maxSize: 1000000) { (data, error) in
            if let err = error {
                onError(err)
            } else {
                onSuccess(data!)
            }
        }
    }
    
    static func uploadPofilImage(image: UIImage, onSuccess: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) {
        let ref = storage.reference(withPath: "profil_images/\(STGAuth.getUID()).jpg")
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        guard let imageData = image.jpegData(compressionQuality: 0.05) else {
            Utils.log(logType: .error, text: "Hochladen des Bildes fehlgeschlagen; Fehlercode: jpegData", logLocation: "Backend", code: nil, error: nil)
            return
        }
        
        ref.putData(imageData, metadata: metadata) { (metadata, error) in
            if let err = error {
                onError(err)
            } else {
                onSuccess()
            }
        }
    }
    
    static func deleteProfilImage(onSuccess: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) {
        storage.reference(withPath: "profil_images/\(Auth.auth().currentUser?.uid ?? "").jpg").delete { (error) in
            if let err = error {
                onError(err)
            } else {
                onSuccess()
            }
        }
    }
}
