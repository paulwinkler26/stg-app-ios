//
//  AccountViewController.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 11.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit
import JGProgressHUD

class AccountViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    // MARK: Lets and vars
    let hud = JGProgressHUD(style: .dark)

    // MARK: Outlets
    @IBOutlet weak var profilImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var uidLabel: UILabel!
    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var backIcon: UIBarButtonItem!
    @IBOutlet weak var changeProfilImageView: UIView!

    // MARK: App Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        setupDesign()
        setupData()

        // Back button for iOS < 13
        if #available(iOS 13.0, *) {} else {
            backIcon.image = UIImage(named: "backIconSmall")
        }
        
        changeProfilImageView.isHidden = UserDefaults.standard.string(forKey: "group")! == "Schüler"
    }

    // MARK: Actions
    @IBAction func backButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func helpButtonTapped(_ sender: Any) {
        guard let url = URL(string: "http://stgapp.de/hilfe/was-sind-raenge") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func changePassword(_ sender: Any) {
        performSegue(withIdentifier: "changePassword", sender: nil)
    }
    
    @IBAction func changeProfilImage(_ sender: Any) {
        let alert = UIAlertController(title: "Profilbild auswählen", message: "Willst du ein neues Profilbild auswählen?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Neues Profilbild auswählen", style: .default, handler: { (action) in
            self.getPicture()
        }))
        alert.addAction(UIAlertAction(title: "Profilbild löschen", style: .default, handler: { (action) in
            self.hud.textLabel.text = "Loading"
            self.hud.show(in: self.view)
            
            self.deleteProfilImage()
        }))
        alert.addAction(UIAlertAction(title: "Abbrechen", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

    // Setup
    func setupDesign() {
        profilImageView.layer.masksToBounds = false
        profilImageView.layer.cornerRadius = profilImageView.frame.size.width/2
        profilImageView.clipsToBounds = true
    }
    
    func setupData() {
        nameLabel.text = UserDefaults.standard.string(forKey: "name")!
        uidLabel.text = STGAuth.getUID()
        groupLabel.text = UserDefaults.standard.string(forKey: "group")!
        
        downloadImage()
    }

    // Download profil image from Firebase
    func downloadImage() {
        STGStorage.getImage(path: "profil_images/\(STGAuth.getUID()).jpg", onSuccess: { (data) in
            self.profilImageView.image = UIImage(data: data)
        }) { (err) in
            print("Download des Profilbeldes fehlgeschlagen; Fehlercode: \(err.localizedDescription)")
        }
    }

    // Get new Image from camera library
    func getPicture() {
        let picker = UIImagePickerController()
        
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.allowsEditing = false
        
        present(picker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: nil)
        
        if let image = info[.originalImage] as? UIImage {
            
            self.hud.textLabel.text = "Loading"
            self.hud.show(in: self.view)
            
            uploadImage(image: image)
        } else {
            hud.show(in: self.view)
            
            hud.textLabel.text = "Fehler beim Zugreifen auf deine Photobibliotek"
            hud.indicatorView = JGProgressHUDErrorIndicatorView()
            hud.dismiss(afterDelay: 3)
        }
    }

    // Upload new profilimage to firebase
    func uploadImage(image: UIImage) {
        STGStorage.uploadPofilImage(image: image, onSuccess: {
            self.profilImageView.image = image
            
            self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            self.hud.textLabel.text = "Erfolgreich"
            self.hud.show(in: self.view)
            self.hud.dismiss(afterDelay: 2)
        }) { (err) in
            print("Hochladen des Bildes fehlgeschlagen; Fehlercode: \(err.localizedDescription)")
            self.hud.show(in: self.view)
            
            self.hud.textLabel.text = "Ein Fehler ist beim Hochladen des Bildes aufgetreten"
            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
            self.hud.dismiss(afterDelay: 3)
        }
    }

    // Delete profil mage from Firebase
    func deleteProfilImage() {
        STGStorage.deleteProfilImage(onSuccess: {
            self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            self.hud.textLabel.text = "Erfolgreich"
            self.hud.show(in: self.view)
            self.hud.dismiss(afterDelay: 2)
            
            self.profilImageView.image = UIImage(named: "person-placeholder")
        }) { (err) in
            print("Error while deleting profil image. Error: \(err.localizedDescription)")
            self.hud.show(in: self.view)
            self.hud.textLabel.text = "Ein Fehler ist beim Löschen des Bildes aufgetreten"
            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
            self.hud.dismiss(afterDelay: 3)
        }
    }
}
