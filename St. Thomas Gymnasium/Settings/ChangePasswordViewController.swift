//
//  ChangePasswordViewController.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 16.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit
import JGProgressHUD

class ChangePasswordViewController: UIViewController {

    // MARK: Lets and Vars
    let hud = JGProgressHUD(style: .dark)

    // MARK: Outlets
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var downButton: UIView!
    @IBOutlet weak var oldPasswordView: UIView!
    @IBOutlet weak var oldPasswordTextView: UITextField!
    @IBOutlet weak var newPasswordView: UIView!
    @IBOutlet weak var newPasswordTextView: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var changePasswordButton: UIButton!

    // MARK: App Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        fadeIn()
        setupUI()

        downButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.dismissPopUp (_:))))
    }

    // Handle editing actions
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if oldPasswordTextView.isFirstResponder || newPasswordTextView.isFirstResponder {
            view.endEditing(true)
            return
        }
        
        let touch = touches.first!
        let location = touch.location(in: view)
        
        if location.y < view.frame.height - 350 {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func dismissPopUp(_ sender:UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }

    // MARK: Actions
    @IBAction func changePasswordTapped(_ sender: UIButton) {
        changePassword()
    }
    
    @IBAction func oldPasswordPrimaryAction() {
        newPasswordTextView.becomeFirstResponder()
    }
    
    @IBAction func newPasswordPrimaryAction() {
        changePassword()
    }

    @IBAction func viewIsOnDrag(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: self.view)
        
        if let viewToDrag = sender.view {
            if (viewToDrag.center.y + translation.y) < (view.frame.height - 150) {
                return
            } else {
                viewToDrag.center = CGPoint(x: viewToDrag.center.x,
                y: viewToDrag.center.y + translation.y)
            }
            
            sender.setTranslation(CGPoint(x: 0, y: 0), in: viewToDrag)
            
            let move = Int((viewToDrag.center.y + translation.y) - viewToDrag.center.y)
            if move > 25 {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }

    // Check and change password
    func changePassword() {
        if oldPasswordTextView.text != "" && newPasswordTextView.text != "" {
            if newPasswordTextView.text!.count < 6 {
                showError(content: "Dein Passwort muss mindestens 6 Ziffern haben")
                return
            }
            
            hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
            hud.show(in: self.view)
            hud.textLabel.text = "Loading"
            
            STGAuth.changePassword(password: oldPasswordTextView.text!, newPassword: self.newPasswordTextView.text!, onSuccess: {
                self.hud.textLabel.text = "Passwort geändert"
                self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                self.hud.dismiss(afterDelay: 3)
                
                Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { (timer) in
                    self.dismiss(animated: true, completion: nil)
                }
            }) { (err) in
                print("Couldn't change password. Error: \(err?.localizedDescription ?? "nil")")
                self.showError(content: "Fülle alle Felder aus")
            }
        } else {
            showError(content: "Fülle alle Felder aus")
        }
    }

    // Helper functions
    func showError(content: String) {
        hud.show(in: self.view)
        
        hud.textLabel.text = content
        hud.indicatorView = JGProgressHUDErrorIndicatorView()
        hud.dismiss(afterDelay: 3)
    }
    
    func fadeIn() {
        UIView.animate(withDuration: 0.5, delay: 2, options: .curveEaseIn, animations: {
            self.backgroundView.alpha = 0
        }, completion: nil)
    }
    
    func setupUI() {
        [changePasswordButton, oldPasswordView, newPasswordView].forEach { button in
            button?.layer.cornerRadius = 23
            button?.addPrecastShadow()
        }

        popUpView.layer.cornerRadius = 15
        popUpView.addShadowWithRGB(ra: 5, wx: 0, wy: 0, r: 0, g: 0, b: 0, a: 0.5)
        
        oldPasswordTextView.attributedPlaceholder = NSAttributedString(string: "123456", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "PopUpTextFieldText")!])
        newPasswordTextView.attributedPlaceholder = NSAttributedString(string: "654321", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "PopUpTextFieldText")!])
        
        downButton.layer.cornerRadius = 2
    }
    
}
