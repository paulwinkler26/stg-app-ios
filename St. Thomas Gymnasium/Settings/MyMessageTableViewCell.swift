//
//  MyMessageTableViewCell.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 19.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit
import JGProgressHUD

class MyMessageTableViewCell: UITableViewCell {

    // MARK: Lets and vars
    var parentVC: MessagesViewController?
    var documentID: String?

    // MARK: Outlets
    @IBOutlet weak var messageTitle: UILabel!
    @IBOutlet weak var deleteButton: UIButton!

    // MARK: App Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    // MARK: Actions
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        STGFirestore.deleteOwnMessage(id: documentID!) {
            self.parentVC?.downloadMessages()
        } onError: {
            self.parentVC?.hud.indicatorView = JGProgressHUDErrorIndicatorView()
            self.parentVC?.hud.textLabel.text = "Fehler"
            self.parentVC?.hud.show(in: self)
            self.parentVC?.hud.dismiss(afterDelay: 2)
        }
    }

    // Setup
    func setup(title: String, _documentID: String, parent: MessagesViewController) {
        messageTitle.text = title
        parentVC = parent
        documentID = _documentID
    }

}
