//
//  SettingsViewController.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 10.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit
import MessageUI
import JGProgressHUD

class SettingsViewController: UIViewController, MFMailComposeViewControllerDelegate {

    // MARK: Lets and vars
    let hud = JGProgressHUD(style: .dark)
    var mother: MainViewController?

    // MARK: Outlets
    @IBOutlet weak var profilView: UIView!
    @IBOutlet weak var profilImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var rangAndUIDLabel: UILabel!
    @IBOutlet weak var accountForwardImageView: UIImageView!
    @IBOutlet weak var messagesView: UIView!
    @IBOutlet weak var messagesImageView: UIImageView!
    @IBOutlet weak var licenseButton: UIButton!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var dataProtectionButton: UIButton!
    @IBOutlet weak var notificationsSwitch: UISwitch!
    @IBOutlet weak var themesSegmentControl: UISegmentedControl!
    @IBOutlet weak var googleAnalyticsSwitch: UISwitch!
    @IBOutlet weak var logOutButton: UIButton!
    @IBOutlet weak var reportBugButton: UIButton!

    // MARK: App Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        setupUI()
        addTapRecognizer()
        setupData()

        // Adapt Segments for iOS < 13
        if #available(iOS 13.0, *) {
            themesSegmentControl.isEnabled = true
            themesSegmentControl.selectedSegmentIndex = UserDefaults.standard.integer(forKey: "colorMode")
        }
    }

    // MARK: Actions
    @IBAction func licenseButtonTapped(_ sender: UIButton) {
        guard let url = URL(string: "http://stgapp.de/hilfe/rechtliches/lizenzen") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func dataProtectionTapped(_ sender: UIButton) {
        guard let url = URL(string: "http://stgapp.de/hilfe/rechtliches/datenschutz") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func logOutButtonTapped(_ sender: UIButton) {
        if STGAuth.checkIfUserIsLoggedIn() {
            hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
            hud.textLabel.text = "Loading..."
            hud.show(in: view)
            
            Timer.scheduledTimer(withTimeInterval: 1.7, repeats: false) { (t) in
                self.signOut()
            }
        } else {
            let alert = UIAlertController(title: "Passwort zurücksetzten", message: "Bitte gib hier deine Schul E-Mail Adresse ein. Wir senden dir anschließend eine E-Mail, mit der du dein Passwort zurücksetzen kannst", preferredStyle: .alert)

            alert.addTextField { (textField) in
                textField.placeholder = "max.muster@thomas-gymnasium.de"
            }

            alert.addAction(UIAlertAction(title: "Abbrechnen", style: .destructive, handler: { (alert) in
                return
            }))
            
            alert.addAction(UIAlertAction(title: "Senden", style: .default, handler: { (_) in
                let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
                let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
                let email = alert.textFields?[0].text
                
                if email != nil && emailPred.evaluate(with: email) {
                    STGAuth.resetPassword(email: email!) {
                        self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                        self.hud.textLabel.text = "Email versendet"
                        self.hud.show(in: self.view)
                        self.hud.dismiss(afterDelay: 3)
                    } onError: { (err) in
                        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                        self.hud.textLabel.text = "Bitte gib eine gültige E-mail ein. Die Eingegebene ist nicht gültig oder nicht registriert"
                        self.hud.show(in: self.view)
                        self.hud.dismiss(afterDelay: 10)
                    }
                } else {
                    Utils.log(logType: .warn, text: "No valid email was given", logLocation: "SettingsPasswordVC", code: nil, error: nil)
                    self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
                    self.hud.textLabel.text = "Bitte gib eine gültige E-mail ein. Die Eingegebene ist nicht gültig oder nicht registriert"
                    self.hud.show(in: self.view)
                    self.hud.dismiss(afterDelay: 10)
                }
            }))

            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @available(iOS 13.0, *) // Only avaible for iOS 13
    @IBAction func themeSegmentChanged(_ sender: UISegmentedControl) {
        UserDefaults.standard.set(sender.selectedSegmentIndex, forKey: "colorMode")
        
        UIView.animate(withDuration: 0.3, animations: {
            if sender.selectedSegmentIndex == 1 {
                UIApplication.shared.windows.forEach { window in
                    window.overrideUserInterfaceStyle = .dark
                }
            } else {
                UIApplication.shared.windows.forEach { window in
                    window.overrideUserInterfaceStyle = .unspecified
                }
            }
        }, completion: nil)
    }
    
    @IBAction func reportBugButtonTapped(_ sender: UIButton) {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients(["paul.winkler@thomas-gymnasium.de", "benjamin.meyer@thomas-gymnasium.de"])
        mailComposerVC.setSubject("App Bug Report - iOS - \(Date().shortString(fromDate: Date()).replacingOccurrences(of: ".", with: "-")) - \(STGAuth.getUID())")
        mailComposerVC.setMessageBody("", isHTML: false)
        self.present(mailComposerVC, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        self.hud.textLabel.text = "Vielen Dank fürs Melden. Wir bemühen uns, das Problem schnellstmöglich zu beheben"
        self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 5)
    }
    
    @IBAction func notificationsSwitchChanged() {
        if notificationsSwitch.isOn {
            if STGAuth.checkIfUserIsLoggedIn() {
                STGMessaging.subscribe(to: UserDefaults.standard.string(forKey: "klasse")!)
            } else {
                STGMessaging.subscribe(to: "stundenplan")
                STGMessaging.subscribe(to: "vertretungen")
            }
        } else {
            if STGAuth.checkIfUserIsLoggedIn() {
                STGMessaging.unsubscribe(from: UserDefaults.standard.string(forKey: "klasse")!)
            } else {
                STGMessaging.unsubscribe(from: "stundenplan")
                STGMessaging.unsubscribe(from: "vertretungen")
            }
        }
        
        UserDefaults.standard.set(notificationsSwitch.isOn, forKey: "notificationsEnabled")
    }
    
    @IBAction func googleAnalyticsSwitchChanged() {
        UserDefaults.standard.set(googleAnalyticsSwitch.isOn, forKey: "googleAnalytics")
    }
    
    func setupUI() {
        profilImageView.layer.masksToBounds = false
        profilImageView.layer.cornerRadius = profilImageView.frame.size.width/2
        profilImageView.clipsToBounds = true
    }
    
    func addTapRecognizer() {
        profilView.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.accountForwardImageViewTapped(_:))))
        messagesView.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.messagesImageViewTapped(_:))))
    }
    
    @objc func accountForwardImageViewTapped(_ sender:UITapGestureRecognizer) {
        if !STGAuth.checkIfUserIsLoggedIn() {
            (parent as! MainViewController).performSegue(withIdentifier: "loginSegue", sender: nil)
        } else {
            performSegue(withIdentifier: "accountSegue", sender: nil)
        }
    }
    
    @objc func messagesImageViewTapped(_ sender:UITapGestureRecognizer) {
        if !STGAuth.checkIfUserIsLoggedIn() {
            (parent as! MainViewController).performSegue(withIdentifier: "loginSegue", sender: nil)
        } else {
            if UserDefaults.standard.string(forKey: "group")! != "Schüler" {
                performSegue(withIdentifier: "messagesSegue", sender: nil)
            } else {
                let alert = UIAlertController(title: "Fehlende Berechtigung", message: "Dir fehlt die entsprechende Berechtigung, um Nachrichten zu schreiben/verwalten", preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

                self.present(alert, animated: true)
            }
        }
    }
    
    func setupData() {
        if STGAuth.checkIfUserIsLoggedIn() {
            if UserDefaults.standard.string(forKey: "name") == nil {
                usernameLabel.text = "Loading..."
            } else {
                usernameLabel?.text = UserDefaults.standard.string(forKey: "name") ?? "<Interner Fehler>"
            }
            
            rangAndUIDLabel?.text = "\(UserDefaults.standard.string(forKey: "klasse")!) - \(UserDefaults.standard.string(forKey: "group")!)"
        } else {
            usernameLabel.text = "Unbekannter Nutzer"
            rangAndUIDLabel.text = "Logge dich hier ein"
        }
        
        notificationsSwitch?.isOn = UserDefaults.standard.bool(forKey: "notificationsEnabled")
        themesSegmentControl?.selectedSegmentIndex = UserDefaults.standard.integer(forKey: "themeSegmentIndex")
        versionLabel?.text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        googleAnalyticsSwitch?.isOn = UserDefaults.standard.bool(forKey: "googleAnalytics")
        
        downloadImage()
        
        if !STGAuth.checkIfUserIsLoggedIn() { logOutButton.setTitle("Passwort zurücksetzen", for: .normal) }
    }
    
    func downloadImage() {
        let uid = STGAuth.getUID()
        
        guard uid != "" else { return }
        
        STGStorage.getImage(path: "profil_images/\(STGAuth.getUID()).jpg", onSuccess: { (data) in
            self.profilImageView.image = UIImage(data: data)
        }) { (err) in
            print("Download des Profilbeldes fehlgeschlagen; Fehlercode: \(err.localizedDescription)")
        }
    }
    
    func signOut() {
        if STGAuth.logOut() {
            STGMessaging.unsubscribe(from: UserDefaults.standard.string(forKey: "klasse")!)
            
            UserDefaults.standard.removeObject(forKey: "name")
            UserDefaults.standard.removeObject(forKey: "group")
            UserDefaults.standard.removeObject(forKey: "klasse")
            UserDefaults.standard.removeObject(forKey: "googleAnalytics")
            UserDefaults.standard.removeObject(forKey: "notificationsEnabled")
            UserDefaults.standard.removeObject(forKey: "themeSegmentIndex")
            
            UserDefaults.standard.set(true, forKey: "googleAnalytics")
            UserDefaults.standard.set(true, forKey: "notificationsEnabled")
            UserDefaults.standard.set(0, forKey: "themeSegmentIndex")
            
            self.hud.textLabel.text = "Du wurdest abgemeldet. Bitte bedenke, dass du jetzt die App nur noch eingeschränkt benutzen kannst"
            self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            self.hud.dismiss(afterDelay: 8)
            self.mother!.reloadAllData()
        } else {
            self.hud.textLabel.text = "Beim Abmelden ist ein Unbekannter Fehler aufgetreten"
            self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
            self.hud.dismiss(afterDelay: 4)
        }
    }
}
