//
//  MessagesViewController.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 19.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit
import JGProgressHUD

class MessagesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // MARK: Lets and vars
    var myMessages = [MyMessage]()
    let hud = JGProgressHUD(style: .dark)

    // MARK: Outlets
    @IBOutlet weak var newMessageButton: UIButton!
    @IBOutlet weak var messagesTableView: UITableView!
    @IBOutlet weak var backIcon: UIBarButtonItem!

    // MARK: App Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        messagesTableView.delegate = self
        messagesTableView.dataSource = self
        messagesTableView.rowHeight = UITableView.automaticDimension
        messagesTableView.estimatedRowHeight = 800
        
        downloadMessages()
        
        if #available(iOS 13.0, *) {} else {
            backIcon.image = UIImage(named: "backIconSmall")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "writeNewMessage" {
            let destination = segue.destination as! WriteNewMessageViewController
            
            destination.parentVC = self
        }
    }

    // MARK: Actions
    @IBAction func backButtonPressed(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func writeNewMessagePressed() {
        performSegue(withIdentifier: "writeNewMessage", sender: nil)
    }

    // Download messages from Firebase
    func downloadMessages() {
        let isAdmin = UserDefaults.standard.string(forKey: "group")!.elementsEqual("Admin")
        
        STGFirestore.downloadOwnMessages(userIsAdmin: isAdmin) { (myMessages) in
            self.myMessages = myMessages
            self.messagesTableView.reloadData()
        }
    }

    // MARK: Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { myMessages.count }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = messagesTableView.dequeueReusableCell(withIdentifier: "myMessage") as! MyMessageTableViewCell
        
        cell.setup(title: myMessages[indexPath.row].text, _documentID: myMessages[indexPath.row].documentID, parent: self)
        
        return cell
    }
}
