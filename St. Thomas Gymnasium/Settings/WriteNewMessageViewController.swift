//
//  WriteNewMessageViewController.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 19.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit
import JGProgressHUD

class WriteNewMessageViewController: UIViewController {

    // MARK: Lets and vars
    let hud = JGProgressHUD(style: .dark)
    var buttons = [ClassButton]()
    var parentVC: MessagesViewController?

    // MARK: Outlets
    @IBOutlet weak var b5a: UIButton!
    @IBOutlet weak var b5b: UIButton!
    @IBOutlet weak var b5c: UIButton!
    @IBOutlet weak var b5d: UIButton!
    @IBOutlet weak var b6a: UIButton!
    @IBOutlet weak var b6b: UIButton!
    @IBOutlet weak var b6c: UIButton!
    @IBOutlet weak var b7a: UIButton!
    @IBOutlet weak var b7b: UIButton!
    @IBOutlet weak var b7c: UIButton!
    @IBOutlet weak var b8a: UIButton!
    @IBOutlet weak var b8b: UIButton!
    @IBOutlet weak var b8c: UIButton!
    @IBOutlet weak var b9a: UIButton!
    @IBOutlet weak var b9b: UIButton!
    @IBOutlet weak var b9c: UIButton!
    @IBOutlet weak var b10a: UIButton!
    @IBOutlet weak var b10b: UIButton!
    @IBOutlet weak var b10c: UIButton!
    @IBOutlet weak var bq11: UIButton!
    @IBOutlet weak var bq12: UIButton!
    
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var downView: UIView!
    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var textViewActualText: UITextView!
    @IBOutlet weak var sendButton: UIButton!

    // MARK: App Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        buttons = [
            ClassButton(button: b5a, activated: false),
            ClassButton(button: b5b, activated: false),
            ClassButton(button: b5c, activated: false),
            ClassButton(button: b5d, activated: false),
            ClassButton(button: b6a, activated: false),
            ClassButton(button: b6b, activated: false),
            ClassButton(button: b6c, activated: false),
            ClassButton(button: b7a, activated: false),
            ClassButton(button: b7b, activated: false),
            ClassButton(button: b7c, activated: false),
            ClassButton(button: b8a, activated: false),
            ClassButton(button: b8b, activated: false),
            ClassButton(button: b8c, activated: false),
            ClassButton(button: b9a, activated: false),
            ClassButton(button: b9b, activated: false),
            ClassButton(button: b9c, activated: false),
            ClassButton(button: b10a, activated: false),
            ClassButton(button: b10b, activated: false),
            ClassButton(button: b10c, activated: false),
            ClassButton(button: bq11, activated: false),
            ClassButton(button: bq12, activated: false),
        ]

        // Setup
        setupButtons()
        fadeIn()
        setupUI()

        downView.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.dismissPopUp (_:))))
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if textViewActualText.isFirstResponder {
            view.endEditing(true)
            return
        }
        
        let touch = touches.first!
        let location = touch.location(in: view)
        
        if location.y < view.frame.height - 580 {
            dismiss(animated: true, completion: nil)
        }
    }

    // MARK: Actions
    @IBAction func viewIsOnDrag(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: self.view)
        
        if let viewToDrag = sender.view {
            if (viewToDrag.center.y + translation.y) < (view.frame.height - 230) {
                return
            } else {
                viewToDrag.center = CGPoint(x: viewToDrag.center.x,
                y: viewToDrag.center.y + translation.y)
            }
            
            sender.setTranslation(CGPoint(x: 0, y: 0), in: viewToDrag)
            
            let move = Int((viewToDrag.center.y + translation.y) - viewToDrag.center.y)
            if move > 25 {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func dismissPopUp(_ sender:UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonPressed(_ sender: UIButton, forEvent event: UIEvent) {
        buttons[sender.tag].activated = !buttons[sender.tag].activated
        
        setupButtons()
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        if textViewActualText.text == "" {
            showError(content: "Du hast keinen Text ausgewählt")
            return
        }
        
        var activatedClasses = 0
        var activeClasses = [String]()
        
        
        buttons.forEach { (c) in
            if c.activated {
                activatedClasses += 1
                activeClasses.append(c.className!)
            }
        }
        
        if activatedClasses < 1 {
            showError(content: "Du hast keine Klasse ausgewählt")
            return
        }
        
        self.hud.show(in: self.view)
        hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        hud.textLabel.text = "Loading..."
        
        STGFirestore.sendMessage(messageData: [
            "datum": Date().shortString(fromDate: Date()).replacingOccurrences(of: ".", with: "-"),
            "klasse": activeClasses,
            "name": UserDefaults.standard.string(forKey: "name")!,
            "text": textViewActualText.text!,
            "uid": STGAuth.getUID()
        ]) {
            self.hud.textLabel.text = "Gesendet"
            self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            self.hud.dismiss(afterDelay: 3)
            
            Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { (timer) in
                self.dismiss(animated: true) {
                    self.parentVC!.downloadMessages()
                }
            }
        } onError: { (err) in
            self.showError(content: "Ein Interner Fehler ist beim Senden der Nachricht aufgetreten")
        }
    }

    // Setup
    func setupButtons() {
        var i = 0

        buttons.forEach { ( classButton) in
            classButton.button.tag = i
            buttons[i].className = (classButton.button.titleLabel?.text?.uppercased())!

            i += 1

            classButton.button.layer.cornerRadius = 13
            classButton.button.layer.borderWidth = 2

            if classButton.activated {
                classButton.button.layer.borderColor = UIColor.green.cgColor
            } else {
                classButton.button.layer.borderColor = UIColor.red.cgColor
            }
        }
    }

    // Helper functions
    func showError(content: String) {
        hud.show(in: self.view)
        
        hud.textLabel.text = content
        hud.indicatorView = JGProgressHUDErrorIndicatorView()
        hud.dismiss(afterDelay: 3)
    }
    
    func fadeIn() {
        UIView.animate(withDuration: 0.5, delay: 2, options: .curveEaseIn, animations: {
            self.backgroundView.alpha = 0
        }, completion: nil)
    }
    
    func setupUI() {
        popUpView.layer.cornerRadius = 15
        popUpView.addShadowWithRGB(ra: 5, wx: 0, wy: 0, r: 0, g: 0, b: 0, a: 0.5)

        [sendButton, textView].forEach { button in
            button?.layer.cornerRadius = 23
            button?.addPrecastShadow()
        }
        
        downView.layer.cornerRadius = 2
    }
}
