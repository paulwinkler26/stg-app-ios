//
//  ContactViewController.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 06.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var formImageView: UIImageView!
    @IBOutlet weak var phoneImageView: UIImageView!
    @IBOutlet weak var webImageView: UIImageView!
    @IBOutlet weak var formLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var webLabel: UILabel!
    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightConstraint: NSLayoutConstraint!
    
    // MARK: App Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addTabRecognizer()
        
        // Design for iOS < 13
        if #available(iOS 13.0, *) {} else {
            leftConstraint.constant = 30
            rightConstraint.constant = 30
        }
    }
    
    func addTabRecognizer() {
        formImageView.isUserInteractionEnabled = true
        formImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.formTapped (_:))))
        formLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.formTapped (_:))))
        
        phoneImageView.isUserInteractionEnabled = true
        phoneImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.phoneTapped (_:))))
        phoneLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.phoneTapped (_:))))
        
        webImageView.isUserInteractionEnabled = true
        webImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.webTapped (_:))))
        webLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.webTapped (_:))))
    }
    
    @objc func formTapped(_ sender:UITapGestureRecognizer) {
        performSegue(withIdentifier: "formSegue", sender: nil)
    }

    @objc func phoneTapped(_ sender:UITapGestureRecognizer) {
        guard let number = URL(string: "tel://" + "0821455812100") else {
            print("Number could not be generated")
            return
        }
        UIApplication.shared.open(number)
    }
    
    @objc func webTapped(_ sender:UITapGestureRecognizer) {
        guard let url = URL(string: "https://thomas-gymnasium.de") else { return }
        UIApplication.shared.open(url)
    }
}
