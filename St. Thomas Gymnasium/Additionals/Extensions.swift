//
//  Extensions.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 31.12.19.
//  Copyright © 2019 Paul Winkler. All rights reserved.
//

import UIKit

// MARK: UIView
extension UIView {
    func addShadowWithRGB(ra: Float, wx: Float, wy: Float, r: Float, g: Float, b: Float, a: Float) {
        self.layer.shadowOffset = CGSize(width: CGFloat(wx), height: CGFloat(wy))
        self.layer.shadowOpacity = a
        self.layer.shadowRadius = CGFloat(ra)
        self.layer.shadowColor = UIColor.init(red: CGFloat(r), green: CGFloat(g), blue: CGFloat(b), alpha: 1).cgColor
        self.layer.masksToBounds = false
    }
    
    func addShadowWidthName(ra: Float, wx: Float, wy: Float, name: String, a: Float) {
        self.layer.shadowOffset = CGSize(width: CGFloat(wx), height: CGFloat(wy))
        self.layer.shadowOpacity = a
        self.layer.shadowRadius = CGFloat(ra)
        self.layer.shadowColor = UIColor(named: name)?.cgColor
        self.layer.masksToBounds = false
    }
    
    func addPrecastShadow() {
        self.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(3))
        self.layer.shadowOpacity = 0.16
        self.layer.shadowRadius = CGFloat(6)
        self.layer.shadowColor = UIColor(named: "StandartShadow")?.cgColor
        self.layer.masksToBounds = false
    }
}

// MARK: String
extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

// MARK: Date
extension Date {
    static let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        
        // make sure the following are the same as that used in the API
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.locale = Locale.current
        
        return formatter
    }()
    
    func shortString(fromDate date: Date) -> String {
        return Date.formatter.string(from: date)
    }
    
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    static var theDayAfterTomorrow: Date { return Date().dayAfterTomorrow }
    static var tomorrow:  Date { return Date().dayAfter }
    var dayAfter: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var dayAfterTomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 2, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    
    private func getMonth(monthAsNumber: String) -> String {
        switch monthAsNumber {
            case "01": return "Jan"
            case "02": return "Feb"
            case "03": return "Mar"
            case "04": return "Apr"
            case "05": return "Mai"
            case "06": return "Jun"
            case "07": return "Jul"
            case "08": return "Aug"
            case "09": return "Sep"
            case "10": return "Okt"
            case "11": return "Nov"
            case "12": return "Dez"
            default: return "{\(monthAsNumber)}"
        }
    }
    
    var todayAsString: String {
        let today = Date().shortString(fromDate: Date())
        let day = String(today.prefix(2))
        let month = getMonth(monthAsNumber: String(today.prefix(5).suffix(2)))
        
        return "\(day). \(month)"
    }
    
    var tomorrowAsString: String {
        let tomorrow = Date().shortString(fromDate: Date().dayAfter)
        let day = String(tomorrow.prefix(2))
        let month = getMonth(monthAsNumber: String(tomorrow.prefix(5).suffix(2)))
        
        return "\(day). \(month)"
    }
    
    var theDayAfterTomorrowAsString: String {
        let theDayAfterTomorrow = Date().shortString(fromDate: Date().dayAfterTomorrow)
        let day = String(theDayAfterTomorrow.prefix(2))
        let month = getMonth(monthAsNumber: String(theDayAfterTomorrow.prefix(5).suffix(2)))
        
        return "\(day). \(month)"
    }
    
    static func today() -> Date {
        return Date()
    }

    func next(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.next, weekday, considerToday: considerToday)
    }

    func previous(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.previous, weekday, considerToday: considerToday)
    }

    func get(_ direction: SearchDirection, _ weekDay: Weekday, considerToday consider: Bool = false) -> Date {

        let dayName = weekDay.rawValue

        let weekdaysName = getWeekDaysInEnglish().map { $0.lowercased() }

        assert(weekdaysName.contains(dayName), "weekday symbol should be in form \(weekdaysName)")

        let searchWeekdayIndex = weekdaysName.firstIndex(of: dayName)! + 1

        let calendar = Calendar(identifier: .gregorian)

        if consider && calendar.component(.weekday, from: self) == searchWeekdayIndex {
            return self
        }

        var nextDateComponent = calendar.dateComponents([.hour, .minute, .second], from: self)
        nextDateComponent.weekday = searchWeekdayIndex

        let date = calendar.nextDate(after: self, matching: nextDateComponent, matchingPolicy: .nextTime, direction: direction.calendarSearchDirection)
        return date!
    }

    func getWeekDaysInEnglish() -> [String] {
        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = Locale(identifier: "en_US_POSIX")
        return calendar.weekdaySymbols
    }

    enum Weekday: String {
        case monday, tuesday, wednesday, thursday, friday, saturday, sunday
    }

    enum SearchDirection {
        case next
        case previous

        var calendarSearchDirection: Calendar.SearchDirection {
            switch self {
            case .next:
                return .forward
            case .previous:
                return .backward
            }
        }
    }
    
}
