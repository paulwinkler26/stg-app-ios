//
//  Structs.swift
//  St. Thomas Gymnasium
//
//  Created by Paul Winkler on 13.06.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import Foundation
import UIKit

// MARK: Home

struct News {
    let bild: String
    let bildTitel: String
    let name: String
    let text: String
    let titel: String
    let id: String
    
    var image: UIImage?
}

struct Message {
    let datum: String
    let klasse: [String]
    let name: String
    let text: String
    let uid: String
}

struct HomeData {
    var news: [News]
    var messages: [Message]
    var messagesForSearch: [Message]
}


// MARK: Vertretungen

struct Substitute {
    let icon: String
    let stunde: String
    let name: String
    let datum: String
}

struct SubstitutesData {
    var todaysSubstitutes: [Substitute]
    var tomorrowsSubstitutes: [Substitute]
    var theDayAfterTomorrowsSubstitutes: [Substitute]
    var filteredSubstitutes: [Substitute]
    var type: SubstituteType
    var searchIsActive: Bool = false
    
    var currentSubstitutes: [Substitute] {
        if !searchIsActive {
            switch type {
            case .today:
                return todaysSubstitutes
            case .tomorrow:
                return tomorrowsSubstitutes
            case .theDayAfterTomorrow:
                return theDayAfterTomorrowsSubstitutes
            }
        } else {
            return filteredSubstitutes
        }
    }
    
    mutating func clear() {
        todaysSubstitutes.removeAll()
        tomorrowsSubstitutes.removeAll()
        theDayAfterTomorrowsSubstitutes.removeAll()
        filteredSubstitutes.removeAll()
    }
}

struct Day {
    var active: Bool
    var label: UILabel
}

// MARK: TimeTable
struct Meal {
    enum Mealtype {
        case type1 // Typ 1 == Mit Vorspeise ohne Nachspeise
        case type2 // Typ 2 == Mit Nachspeise ohne Vorspeise
        case type3 // Typ 3 == Mit Vorspeise und Nachspeise
    }
    
    let vorspeise: String
    let hauptspeise: String
    let nachspeise: String
    let vegetarisch: String
    let date: String
    let day: String
    
    let type: Mealtype
}

struct Stunde {
    let fach: String
    let raum: String
    let lehrer: String
}

// MARK: Settings
struct MyMessage {
    let text: String
    let documentID: String
}

struct ClassButton {
    let button: UIButton
    var activated: Bool
    var className: String?
}
