//
//  Enumerations.swift
//  St. Thomas Gymnasium
//
//  Created by Paul Winkler on 13.06.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import Foundation

// MARK: Substitutes

enum SubstituteType {
    case today, tomorrow, theDayAfterTomorrow
}

enum DateType {
    case today, tomorrow, theDayAfterTomorrow
    
    var dateForFirestore: String {
        switch self {
        case .today:
            return (
                Date().shortString(fromDate: Date()).replacingOccurrences(of: ".", with: "-")
            )
        case .tomorrow:
            return (
                Date().shortString(fromDate: Date().dayAfter).replacingOccurrences(of: ".", with: "-")
            )
        case .theDayAfterTomorrow:
            return (
                Date().shortString(fromDate: Date().dayAfterTomorrow).replacingOccurrences(of: ".", with: "-")
            )
        }
    }
}

enum CurrentTimeTable {
    case speiseplan, timetable
}
