//
//  StundenplanTableViewCell.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 05.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit

class StundenplanTableViewCell: UITableViewCell {

    // MARK: Outlets
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var mainText: UILabel!
    
    // MARK: App Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // Setup -> Revice Data from parent
    func setup(stundenplanTag: [String: [[String: String?]]], day: String) {
        dayLabel.text = day
        
        var text = ""
        for hour in stundenplanTag.keys.sorted(by: { Int($0)! < Int($1)! }) {
            text = "\(text)Stunde \(hour):\n"
            
            for subhour in stundenplanTag[hour]! {
                text = "\(text)\(subhour["fach"]! ?? ""); \(subhour["lehrer"]! ?? ""); \(subhour["raum"]! ?? "")\n"
            }
            
            text = "\(text)\n"
        }
        
        mainText.text = text
    }
}
