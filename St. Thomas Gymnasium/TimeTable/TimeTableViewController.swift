//
//  TimeTableViewController.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 04.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit

class TimeTableViewController: UIViewController {
    
    // MARK: Lets and Vars
    var currentTimeTable: CurrentTimeTable = .speiseplan
    var speiseplan = [Meal]()
    var stundenplan: [String: [String: [[String: String?]]]]? = [:]
    
    // MARK: Outlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var timeTableTableView: UITableView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var backButton: UIButton!
    
    // MARK: App Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup
        setupTableView()
        
        if #available(iOS 13.0, *) {
            backView.isHidden = true
        } else {
            mainView.backgroundColor = UIColor.black
            backView.layer.cornerRadius = backView.frame.width / 2
            backButton.layer.cornerRadius = backButton.frame.width / 2
            backView.addShadowWithRGB(ra: 5, wx: 0, wy: 3, r: 0, g: 0, b: 0, a: 0.8)
            segment.tintColor = UIColor.white
        }
        
        // Download data from Firebase -> Data Handling
        dowloadSpeiseplan()
        downloadStundenplan()
    }
    
    // Show status bar in iOS < 13
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 13.0, *) {} else {
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .default
        } else {
            return .lightContent
        }
    }

    // MARK: Actions
    // Shown segment changed
    @IBAction func segmentChanged(_ sender: UISegmentedControl) {
        currentTimeTable = sender.selectedSegmentIndex == 0 ? .speiseplan : .timetable
        timeTableTableView.reloadData()
    }
    
    // Dismiss button pressen (only for iOS < 13)
    @IBAction func backButtonPressed() {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Data Handling
extension TimeTableViewController {
    func dowloadSpeiseplan() {
        STGFirestore.getMealPlan(onSuccess: { (meals) in
            self.speiseplan = meals
            if self.currentTimeTable == .speiseplan { self.timeTableTableView.reloadData() }
        }) { (err) in
            print("Beim Runterladen des Speiseplans ist ein Fehler aufgetreten")
        }
    }
    
    func downloadStundenplan() {
        guard STGAuth.checkIfUserIsLoggedIn() else { return }
        
        print("Downloading TimeTable")
        STGFirestore.getTimeTableV2 { (stundenplan) in
            self.stundenplan = stundenplan
            print("Successfully downloaded stundenplan")
        } onError: { (err) in
            print("Error while downloading stundenplan")
        }

    }
}

// MARK: - Table View
extension TimeTableViewController: UITableViewDelegate, UITableViewDataSource {
    func setupTableView() {
        timeTableTableView.delegate = self
        timeTableTableView.dataSource = self
        
        timeTableTableView.rowHeight = UITableView.automaticDimension
        timeTableTableView.estimatedRowHeight = 800
        
        timeTableTableView.separatorStyle = .none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch currentTimeTable {
        case .speiseplan:
            return self.speiseplan.count == 0 ? 1 : self.speiseplan.count
        case .timetable:
            return self.stundenplan == nil || (self.stundenplan != nil && self.stundenplan!.isEmpty) ? 1 : 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch currentTimeTable {
        case .speiseplan:
            return self.speiseplan.count == 0 ? timeTableTableView.dequeueReusableCell(withIdentifier: "keineDaten")! : { () -> UITableViewCell in
                let cell = timeTableTableView.dequeueReusableCell(withIdentifier: "speisetag") as! SpeisetagTableViewCell
                
                cell.setup(meal: self.speiseplan[indexPath.row])
                
                return cell
            }()
        case .timetable:
            return self.stundenplan == nil || (self.stundenplan != nil && self.stundenplan!.isEmpty) ? timeTableTableView.dequeueReusableCell(withIdentifier: "keineDaten")! : { () -> UITableViewCell in
                let cell = timeTableTableView.dequeueReusableCell(withIdentifier: "stundenplan") as! StundenplanTableViewCell
                
                switch indexPath.row {
                case 0:
                    cell.setup(stundenplanTag: self.stundenplan!["Montag"]!, day: "Montag")
                case 1:
                    cell.setup(stundenplanTag: self.stundenplan!["Dienstag"]!, day: "Dienstag")
                case 2:
                    cell.setup(stundenplanTag: self.stundenplan!["Mittwoch"]!, day: "Mittwoch")
                case 3:
                    cell.setup(stundenplanTag: self.stundenplan!["Donnerstag"]!, day: "Donnerstag")
                case 4:
                    cell.setup(stundenplanTag: self.stundenplan!["Freitag"]!, day: "Freitag")
                default:
                    Utils.log(logType: .error, text: "CRITICAL: Unknown day found", logLocation: "TimeTableViewController", code: nil, error: nil)
                }
                return cell
            }()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0
        
        UIView.animate(
            withDuration: 0.2,
            delay: 0.02 * Double(indexPath.row) + (Double(indexPath.section) * 0.2),
            animations: {
                cell.alpha = 1
        })
    }
}
