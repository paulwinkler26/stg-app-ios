//
//  SpeisetagTableViewCell.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 05.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit

class SpeisetagTableViewCell: UITableViewCell {

    // MARK: Outlets
    @IBOutlet weak var titelLabel: UILabel!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var firstMeal: UILabel!
    @IBOutlet weak var secondMeal: UILabel!
    @IBOutlet weak var thirdMeal: UILabel!
    
    // MARK: App Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // Setup -> Recieve Data from parent
    func setup(meal: Meal) {
        titelLabel.text = "\(meal.day), \(meal.date)"
        
        switch meal.type {
        case .type1:
            firstMeal.text = "Vorspeise: \(meal.vorspeise)"
            secondMeal.text = "Hauptspeise: \(meal.hauptspeise)"
            thirdMeal.text = "Vegetarisch: \(meal.vegetarisch)"
        case .type2:
            firstMeal.text = "Hauptspeise: \(meal.hauptspeise)"
            secondMeal.text = "Nachspeise: \(meal.nachspeise)"
            thirdMeal.text = "Vegetarisch: \(meal.vegetarisch)"
        case .type3:
            firstMeal.text = "Vorspeise: \(meal.vorspeise)"
            secondMeal.text = "Hauptspeise: \(meal.hauptspeise)"
            thirdMeal.text = "Nachspeise: \(meal.nachspeise)"
        }
    }
}
