//
//  SubstituteViewController.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 31.12.19.
//  Copyright © 2019 Paul Winkler. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class SubstituteViewController: UIViewController {
    
    // MARK: - Lets and Vars
    var refresher: UIRefreshControl?
    var dateCell: DateSelectorTableViewCell?
    var mother: MainViewController?
    var data = SubstitutesData(todaysSubstitutes: [], tomorrowsSubstitutes: [], theDayAfterTomorrowsSubstitutes: [], filteredSubstitutes: [], type: .today)

    // MARK: - Outlets
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchViewTextField: UITextField!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var substituteTableView: UITableView!
    
    // MARK: App Life Cycle
    override func viewDidLoad() {
        // Setup
        setupUI()
        
        setupTableView() // Setup Table View -> Table View Extension
        downloadSubstitutes() // Download data from Firebase -> Data Handling Extension
        
        // Analytics
        if UserDefaults.standard.bool(forKey: "googleAnalytics") {
            Analytics.logEvent("view_controller_triggered", parameters: ["view_controller": "SubstituteViewController" as NSObject])
        }
    }
    
    // MARK: Searching
    // Stop Searching by tapping
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) { view.endEditing(true) }
    
    // Stop searching by scrolling
    func scrollViewDidScroll(_ scrollView: UIScrollView) { view.endEditing(true) }
    
    // Called when seach bar is used
    @IBAction func searchChanged(_ sender: UITextField) {
        data.filteredSubstitutes.removeAll()
        
        data.searchIsActive = sender.text == "" ? false : { () -> Bool in
            for substitute in data.currentSubstitutes where substitute.name.contains(sender.text!) || substitute.datum.contains(sender.text!) || substitute.stunde.contains(sender.text!) {
                self.data.filteredSubstitutes.append(substitute)
            }
            return true
        }()
            
        // Analytics
        if UserDefaults.standard.bool(forKey: "googleAnalytics") {
            Analytics.logEvent("search", parameters: ["search_term": sender.text!])
        }
    }
    
    @IBAction func helpButtonIsPressed(_ sender: UIButton) {
        guard let url = URL(string: "http://stgapp.de/hilfe/wie-funktionieren-vertretungen") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func logInButtonPressed(_ sender: UIButton) {
        mother?.performLogin()
    }
    
    // MARK: Design
    func setupUI() {
        searchView.layer.cornerRadius = 23
        searchView.addPrecastShadow()
        
        searchViewTextField.attributedPlaceholder = NSAttributedString(string: "Suchen", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "GreyText")!])
    }
    
    // MARK: - Data Handling
    func downloadSubstitutes() {
        guard STGAuth.checkIfUserIsLoggedIn(), let klasse = UserDefaults.standard.string(forKey: "klasse") else {
            self.refresher?.endRefreshing()
            return
        }
        
        let userIsTeacher = UserDefaults.standard.string(forKey: "group")?.elementsEqual("Lehrer") ?? false
        
        data.clear()
        
        STGFirestore.getSubstitutes(userIsTeacher: userIsTeacher, klasse: klasse) { (substituteData) in
            self.data.todaysSubstitutes = substituteData[0]
            self.data.tomorrowsSubstitutes = substituteData[1]
            self.data.theDayAfterTomorrowsSubstitutes = substituteData[2]
            
            print("Substitute Download finished!")
            
            self.refresher?.endRefreshing()
            self.substituteTableView?.reloadData()
        }
    }
}


// MARK: - Table View
extension SubstituteViewController: UITableViewDelegate, UITableViewDataSource {
    func setupTableView() {
        substituteTableView.delegate = self
        substituteTableView.dataSource = self
        
        substituteTableView.separatorStyle = .none
        
        refresher = UIRefreshControl()
        substituteTableView.addSubview(refresher!)
        refresher!.attributedTitle = NSAttributedString(string: "Aktualisieren", attributes: [.strokeColor : UIColor.black])
        refresher!.addTarget(self, action: #selector(reloadTableView), for: .valueChanged)
    }
    
    @objc func reloadTableView() { downloadSubstitutes() }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.currentSubstitutes.count + 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if !STGAuth.checkIfUserIsLoggedIn() && indexPath.row == 1 {
            return 250
        }
        
        return indexPath.row == 0 ? 135 : indexPath.row == data.currentSubstitutes.count + 1 ? 75 : indexPath.row == data.currentSubstitutes.count + 2 ? 50 : 78
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !data.searchIsActive {
            if indexPath.row == 0 {
                return
            }
            
            cell.alpha = 0
            
            UIView.animate(
                withDuration: 0.5,
                delay: 0.05 * Double(indexPath.row - 1),
                animations: {
                    cell.alpha = 1
            })
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if !STGAuth.checkIfUserIsLoggedIn() && indexPath.row == 1 {
            return tableView.dequeueReusableCell(withIdentifier: "notLoggedIn")!
        }
        
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "dateSelector") as? DateSelectorTableViewCell else {
                print("Date Selector Creation Failed!")
                return UITableViewCell(style: .default, reuseIdentifier: "error")
            }
            
            cell.parentVC = self
            self.dateCell = cell
            
            return cell
        case data.currentSubstitutes.count + 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "noMoreSubstitutes") else {
                print("noMoreSubstitutes Creation Failed!")
                return UITableViewCell(style: .default, reuseIdentifier: "error")
            }
            
            return cell
        case data.currentSubstitutes.count + 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "whatDoesItMean") else {
                print("whatDoesItMean Creation Failed!")
                return UITableViewCell(style: .default, reuseIdentifier: "error")
            }
            
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "substitute") as? SubstituteTableViewCell else {
                print("Substitute Creation Failed!")
                return UITableViewCell(style: .default, reuseIdentifier: "error")
            }
            
            guard data.currentSubstitutes.count >= (indexPath.row - 1) else {
                DispatchQueue.main.async {
                    self.substituteTableView?.reloadData()
                }
                
                print("strange error happenend")
                return cell
            }

            cell.initialise(substitute: data.currentSubstitutes[indexPath.row - 1])
            
            return cell
        }
    }
}
