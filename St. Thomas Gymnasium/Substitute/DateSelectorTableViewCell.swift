//
//  DateSelectorTableViewCell.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 31.12.19.
//  Copyright © 2019 Paul Winkler. All rights reserved.
//

import UIKit

class DateSelectorTableViewCell: UITableViewCell {
    
    var date: DateType = .today
    var parentVC: SubstituteViewController?
    var fields = [UIView : Day]()
    
    // MARK: - Outlets
    @IBOutlet weak var dateStackView: UIStackView!
    @IBOutlet weak var todayView: UIView!
    @IBOutlet weak var tomorrowView: UIView!
    @IBOutlet weak var theDayAfterTomorrowView: UIView!
    @IBOutlet weak var todayLabel: UILabel!
    @IBOutlet weak var tomorrowLabel: UILabel!
    @IBOutlet weak var theDayAfterTomorrowLabel: UILabel!
    
    // MARK: App Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        fields = [todayView : Day(active: true, label: todayLabel), tomorrowView : Day(active: false, label: tomorrowLabel), theDayAfterTomorrowView : Day(active: false, label: theDayAfterTomorrowLabel)]
        
        setupUI()
        addTapRecognizers()
    }
    
    // MARK: - Setup UI
    func setupUI() {
        todayLabel.text = Date().todayAsString
        tomorrowLabel.text = Date().tomorrowAsString
        theDayAfterTomorrowLabel.text = Date().theDayAfterTomorrowAsString
        
        
        fields.forEach { (key, value) in
            if value.active {
                key.layer.backgroundColor = UIColor(named: "DateViewActive")?.cgColor
                value.label.textColor = UIColor(named: "TextInActiveDateView")
            } else {
                key.layer.backgroundColor = UIColor(named: "DateViewInactive")?.cgColor
                value.label.textColor = UIColor(named: "TextInInactiveDateView")
            }
            
            key.layer.cornerRadius = 25
            key.addShadowWidthName(ra: 8, wx: 0, wy: 3, name: "StandartShadow", a: 0.24)
        }
    }
    
    func updateUI() {
        fields.forEach { (key, value) in
            if value.active {
                key.layer.backgroundColor = UIColor(named: "DateViewActive")?.cgColor
                value.label.textColor = UIColor(named: "TextInActiveDateView")
            } else {
                key.layer.backgroundColor = UIColor(named: "DateViewInactive")?.cgColor
                value.label.textColor = UIColor(named: "TextInInactiveDateView")
            }
        }
    }
    
    func addTapRecognizers() {
        todayView.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.todayViewTapped (_:))))
        tomorrowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.tomorrowViewTapped (_:))))
        theDayAfterTomorrowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector (self.theDayAfterTomorrowViewTapped (_:))))
    }
        
    @objc func todayViewTapped(_ sender:UITapGestureRecognizer) {
        fields[todayView]!.active = true
        fields[tomorrowView]!.active = false
        fields[theDayAfterTomorrowView]!.active = false
        updateUI()
        
        parentVC?.data.type = .today
        parentVC?.substituteTableView.reloadData()
    }
    
    @objc func tomorrowViewTapped(_ sender:UITapGestureRecognizer) {
        fields[todayView]?.active = false
        fields[tomorrowView]?.active = true
        fields[theDayAfterTomorrowView]?.active = false
        updateUI()
        
        parentVC?.data.type = .tomorrow
        parentVC?.substituteTableView.reloadData()
    }
    
    @objc func theDayAfterTomorrowViewTapped(_ sender:UITapGestureRecognizer) {
        fields[todayView]?.active = false
        fields[tomorrowView]?.active = false
        fields[theDayAfterTomorrowView]?.active = true
        updateUI()
        
        parentVC?.data.type = .theDayAfterTomorrow
        parentVC?.substituteTableView.reloadData()
    }
}
