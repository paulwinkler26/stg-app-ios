//
//  SubstituteTableViewCell.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 31.12.19.
//  Copyright © 2019 Paul Winkler. All rights reserved.
//

import UIKit

class SubstituteTableViewCell: UITableViewCell {

    // MARK: Lets and Vars
    var kind: String?
    var hour: String?
    var teacher: String?
    
    // MARK: - Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var typeView: UIView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var borderOne: UIView!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var teacherLabel: UILabel!
    @IBOutlet weak var borderTwo: UIView!
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUI()
    }
    
    // MARK: - Functions
    func setupUI() {
        mainView.layer.cornerRadius = 20
        mainView.addPrecastShadow()
        
        typeView.layer.cornerRadius = 10
        typeView.addPrecastShadow()
    }
    
    func updateUI() {
        switch kind! {
        case "e":
            typeView.layer.backgroundColor = UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 100/100).cgColor
            typeLabel.textColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 100/100)
            typeLabel.text = "E"
        case "v":
            typeView.layer.backgroundColor = UIColor(red: 255/255, green: 242/255, blue: 92/255, alpha: 100/100).cgColor
            typeLabel.textColor = UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 100/100)
            typeLabel.text = "V"
        case "s":
            typeView.layer.backgroundColor = UIColor(red: 0/255, green: 255/255, blue: 39/255, alpha: 100/100).cgColor
            typeLabel.textColor = UIColor(red: 85/255, green: 85/255, blue: 85/255, alpha: 100/100)
            typeLabel.text = "S"
        default:
            typeView.layer.backgroundColor = UIColor.gray.cgColor
            typeLabel.textColor = UIColor.white
            typeLabel.text = kind!
        }
        
        hourLabel.text = "\(hour ?? "Err") S"
        teacherLabel.text = teacher ?? "Err"
    }
    
    func initialise(substitute: Substitute) {
        kind = substitute.icon
        hour = substitute.stunde
        teacher = substitute.name
        
        updateUI()
    }
}
