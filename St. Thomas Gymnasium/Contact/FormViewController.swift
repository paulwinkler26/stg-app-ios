//
//  FormViewController.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 06.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit
import JGProgressHUD
import FirebaseFirestore
import FirebaseAuth

class FormViewController: UIViewController, UIScrollViewDelegate {

    // MARK: Lets and vars
    let hud = JGProgressHUD(style: .dark)
    let times = UserDefaults.standard.integer(forKey: "formSend")
    
    // MARK: Outlets
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var vornameView: UIView!
    @IBOutlet weak var vornameTextField: UITextField!
    @IBOutlet weak var nachnameView: UIView!
    @IBOutlet weak var nachnameTextField: UITextField!
    @IBOutlet weak var personView: UIView!
    @IBOutlet weak var personTextField: UITextField!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var sentButton: UIButton!
    @IBOutlet weak var backIcon: UIBarButtonItem!
    
    // App Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        mainScrollView.delegate = self
        
        // UI Setup
        setupUI()
        // Back button for iOS < 13
        if #available(iOS 13.0, *) {} else {
            backIcon.image = UIImage(named: "backIconSmall")
        }
    }
    
    // Send form button pressed
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        if vornameTextField.text == "" || nachnameTextField.text == "" || personTextField.text == "" || emailTextField.text == "" || messageTextView?.text ?? "" == "" {
            showError("Bitte fülle alle Felder aus")
            return
        }
        
        if personTextField.text != "Ja" && personTextField.text != "Nein" {
            showError("Bitte gib auf die Frage \"Bist du ein Schüler oder Elternteil\" Ja oder Nein an")
            return
        }
        
        if !isValidEmail(emailTextField.text!) {
            showError("Bitte gib eine korrekte Email Adresse an")
            return
        }
        
        sendEmail()
    }
    
    // Abort button pressed
    @IBAction func backButtonPressed(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Bestätigen", message: "Willst du wirklich abrechen? Alle deine Eingaben werden gelöscht!", preferredStyle: .alert)
        
        
        alert.addAction(UIAlertAction(title: "Ja", style: .destructive, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Nein", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: Actions for Text fields
    @IBAction func vornamgeGoOnPressed(_ sender: Any) {
        nachnameTextField.becomeFirstResponder()
    }
    
    @IBAction func nachnameGoOnPressed(_ sender: UITextField) {
        personTextField.becomeFirstResponder()
    }
    
    @IBAction func personGoOnTriggered(_ sender: Any) {
        emailTextField.becomeFirstResponder()
    }
    
    @IBAction func emailGoOnTriggered(_ sender: Any) {
        messageTextView.becomeFirstResponder()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    // MARK: UI
    func setupUI() {
        [vornameView, nachnameView, personView, emailView, messageView, sentButton].forEach { element in
            element?.layer.cornerRadius = 23
            element?.addPrecastShadow()
        }
    }
    
    // MARK: Rest functions
    // Send email to school
    func sendEmail() {
        let vorname: String = vornameTextField.text!
        let nachname: String = nachnameTextField.text!
        let schuelerBool: String = personTextField.text!
        let email: String = emailTextField.text!
        let nachicht: String = messageTextView.text!
        let username: String = UserDefaults.standard.string(forKey: "name") == nil ? "undefind": UserDefaults.standard.string(forKey: "name")!
        let klasse = UserDefaults.standard.string(forKey: "klasse") == nil ? "undefind": UserDefaults.standard.string(forKey: "klasse")!
        
        Firestore.firestore().collection("formularData").document().setData([
            "vorname": vorname,
            "nachname": nachname,
            "schuelerBool": schuelerBool,
            "email": email,
            "nachricht": nachicht,
            "username": username,
            "klasse": klasse,
            "uid": Auth.auth().currentUser == nil ? "" : Auth.auth().currentUser!.uid
        ]) { error in
            guard error == nil else {
                self.showError("Bei der Kommunikation mit unseren Servern ist ein Fehler aufgetreten. Probiere es später erneut")
                print("Fehler: \(error!.localizedDescription)")
                return
            }
            
            self.hud.textLabel.text = "Daten werden übertragen"
            self.hud.show(in: self.view)

            UserDefaults.standard.set(self.times + 1, forKey: "formSend")
            print("Form gesendet")

            self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
            self.hud.textLabel.text = "Formular gesendet!"

            Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { (timer1) in
                self.hud.textLabel.text = "Du wirst auf deiner Email über alles weitere benachrichtigt!"
                Timer.scheduledTimer(withTimeInterval: 8, repeats: false) { (timer2) in
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }

    // Helper Functions
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }

    func showError(_ content: String) {
        hud.show(in: self.view)

        hud.textLabel.text = content
        hud.indicatorView = JGProgressHUDErrorIndicatorView()
        hud.dismiss(afterDelay: 3)
    }
}
