//
//  HomeHeader.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 03.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit

class HomeHeader: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(headerTitle: String) {
        headerLabel.text = headerTitle
    }
}
