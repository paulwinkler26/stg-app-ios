//
//  HomeViewController.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 03.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class HomeViewController: UIViewController {
    
    // MARK: Lets and Vars
    var data = HomeData(news: [], messages: [], messagesForSearch: [])
    var mother: MainViewController?
    var tranferData: News!
    var downloadTimes = 0
    var refresher: UIRefreshControl?
    var searchIsActive = false
    
    // MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var newsTableView: UITableView!
    
    // MARK: View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()

        checkForFirstLogin()
        setupUI()
        
        setupTableView() // Setup Table View -> Table View extension
        downloadNews() // Download Data from Firebase -> Data Handling
    }
    
    // Checks if this is the first login
    func checkForFirstLogin() {
        if !UserDefaults.standard.bool(forKey: "thisIsTheFirstLogIn") {
            Utils.log(logType: .info, text: "This was the first login", logLocation: "HomeVC", code: nil, error: nil)
            
            UserDefaults.standard.set(true, forKey: "googleAnalytics")
            UserDefaults.standard.set(true, forKey: "notificationsEnabled")
            UserDefaults.standard.set(true, forKey: "thisIsTheFirstLogIn")
        }
    }
    
    func setupUI() {
        searchView.layer.cornerRadius = 23
        searchView.addPrecastShadow()
        searchTextField.attributedPlaceholder = NSAttributedString(string: "Suchen", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "GreyText")!])
    }
    
    // MARK: Segues
    // Prepare for any seque
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Check if the segue points to an article -> send the already downloaded data to the article for saving network capicity
        if segue.identifier == "showArticle" {
            let navigationVC = segue.destination as? UINavigationController
            
            let vc = navigationVC?.children[0] as! ArticleViewController
            vc.data = tranferData!
        }
    }

    // MARK: Actions
    // Function is triggered when search bar is used
    @IBAction func searchBarChanged(_ sender: UITextField) {
        if !data.messagesForSearch.isEmpty { data.messagesForSearch.removeAll() }
        
        // Searches data for matches which the search data
        searchIsActive = sender.text == "" ? false : { () -> Bool in
            for message in self.data.messages where message.text.contains(sender.text!) || message.klasse.contains(sender.text!) || message.name.contains(sender.text!) {
                self.data.messagesForSearch.append(message)
            }
            return true
        }()
        
        newsTableView.reloadData()
    }
}

// MARK: - Data Handling
extension HomeViewController {
    // Download data from Firebase
    func downloadNews() {
        STGFirestore.getNews(onSuccess: { (news) in
            self.data.news = news
            
            self.downloadMessages()
        }) { (err) in
            Utils.log(logType: .error, text: "Could not fetch news from Firebase", logLocation: "HomeVC", code: .firebaseError, error: err)
            self.refresher?.endRefreshing()
        }
    }
    
    func downloadMessages() {
        guard STGAuth.checkIfUserIsLoggedIn() else {
            self.refresher?.endRefreshing()
            self.newsTableView.reloadData()
            return
        }
        
        STGFirestore.getMessages(onSuccess: { (messages) in
            self.data.messages = messages
            
            self.refresher?.endRefreshing()
            self.newsTableView?.reloadData()
        }) { (err) in
            Utils.log(logType: .error, text: "Could not fetch messages from Firebase", logLocation: "HomeVC", code: .firebaseError, error: err)
            
            self.refresher?.endRefreshing()
            self.newsTableView?.reloadData()
        }
    }
}

// MARK: - Table View
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       view.endEditing(true)
    }
    
    func setupTableView() {
        newsTableView.delegate = self
        newsTableView.dataSource = self
        
        newsTableView.separatorStyle = .none
        
        refresher = UIRefreshControl()
        newsTableView.addSubview(refresher!)
        refresher!.attributedTitle = NSAttributedString(string: "Aktualisieren", attributes: [.strokeColor : UIColor.black])
        refresher!.addTarget(self, action: #selector(reloadTableView), for: .valueChanged)
        
        newsTableView.rowHeight = UITableView.automaticDimension
        newsTableView.estimatedRowHeight = 800
    }
    
    @objc func reloadTableView() {
        downloadMessages()
    }
    
    // TODO: Schau dir das mal genauer an
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchIsActive {
            if data.messages.isEmpty {
                return 1
            } else {
                return data.messagesForSearch.count
            }
        } else {
            if data.messages.count != 0 && data.news.count != 0 {
                return section == 0 ? data.news.count : data.messages.count + 1
            } else if data.messages.count == 0 {
                return section == 0 ? 1 : (data.messages.count + 1)
            } else if data.news.count == 0 {
                return section == 0 ? data.news.count : 2
            } else {
                return section == 0 ? 1 : 2
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if searchIsActive {
            return data.messages.isEmpty ? newsTableView.dequeueReusableCell(withIdentifier: "noData")! : indexPath.row != data.messages.count ? { () -> UITableViewCell in
                let cell = newsTableView.dequeueReusableCell(withIdentifier: "message") as! MessageTableViewCell
                
                cell.setupData(data: data.messagesForSearch[indexPath.row], _whiteCell: (indexPath.row % 2) == 1)
                
                return cell
            }() : newsTableView.dequeueReusableCell(withIdentifier: "allViewed")!
        } else {
            if indexPath.section == 0 {
                if data.news.count == 0 {
                    return newsTableView.dequeueReusableCell(withIdentifier: "noData")!
                } else {
                    let cell = newsTableView.dequeueReusableCell(withIdentifier: "news") as! NewsTableViewCell
                    
                    cell.setupData(data: data.news[indexPath.row])
                    cell.parent = self
                    
                    return cell
                }
            } else {
                if data.messages.count == 0 {
                    return newsTableView.dequeueReusableCell(withIdentifier: "noData")!
                } else if indexPath.row != data.messages.count {
                    let cell = newsTableView.dequeueReusableCell(withIdentifier: "message") as! MessageTableViewCell
                    
                    cell.setupData(data: data.messages[indexPath.row], _whiteCell: (indexPath.row % 2) == 1)
                    
                    return cell
                } else {
                    return newsTableView.dequeueReusableCell(withIdentifier: "allViewed")!
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return searchIsActive ? { () -> UIView? in
            let headerView = Bundle.main.loadNibNamed("HomeHeader", owner: self, options: nil)?.first as? HomeHeader
            
            headerView?.setup(headerTitle: section == 0 ? "Aktuelle Neuigkeiten" : "Deine Nachrichten")
            
            return headerView!
        }() : { () -> UIView? in
            let headerView = Bundle.main.loadNibNamed("HomeHeader", owner: self, options: nil)?.first as? HomeHeader
            
            headerView?.setup(headerTitle: "Suchergebnis")
            
            return headerView!
        }()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat { 53 }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return searchIsActive ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !searchIsActive {
            cell.alpha = 0
            
            UIView.animate(
                withDuration: 0.2,
                delay: 0.02 * Double(indexPath.row) + (Double(indexPath.section) * 0.2),
                animations: {
                    cell.alpha = 1
            })
        }
    }
}
