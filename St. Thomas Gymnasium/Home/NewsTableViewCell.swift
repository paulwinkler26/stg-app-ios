//
//  NewsTableViewCell.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 03.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    // MARK: Lets and Vars
    var parent: HomeViewController?
    var completeData: News?
    var titel: String?
    var nText: String?
    var bildTitel: String?
    var bildAsString: String?
    var bildAsImage: UIImage?
    var name: String?
    var id: String?
    
    // MARK: Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var shadowImage: UIImageView!
    @IBOutlet weak var newsTextLabel: UILabel!
    
    // MARK: App Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setupUI()
        setupAction()
    }
    
    // MARK: Functions
    func setupUI() {
        // First View
        mainView.layer.cornerRadius = 15
        mainView.addPrecastShadow()
        
        backgroundImage.layer.cornerRadius = 15
        
        shadowImage.layer.cornerRadius = 15
        shadowImage.alpha = 0.75
    }

    func setupAction() {
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.tapped(_:)))
        mainView.addGestureRecognizer(gesture)
    }
    
    // Function is called by parent to transmit data to cell
    func setupData(data: News) {
        completeData = data
        
        bildAsString = data.bild
        bildTitel = data.bildTitel
        titel = data.titel
        name = data.name
        nText = data.text
        id = data.id
        
        newsTextLabel.text = titel
        
        downloadImage()
    }
    
    // Downloads news image from firebase
    func downloadImage() {
        STGStorage.getImage(path: "news_images/\(bildAsString!).jpg", onSuccess: { (data) in
            self.bildAsImage = UIImage(data: data)
            self.backgroundImage.image = self.bildAsImage
        }) { (err) in
            print("Download der News ist fehlgeschlagen; ID: \(self.id!); Fehlercode: \(err.localizedDescription)")
        }
    }
    
    @objc func tapped(_ sender:UITapGestureRecognizer) {
        parent?.tranferData = completeData!
        parent?.tranferData.image = bildAsImage
        
        parent?.performSegue(withIdentifier: "showArticle", sender: nil)
    }
}
