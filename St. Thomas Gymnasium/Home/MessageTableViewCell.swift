//
//  MessageTableViewCell.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 03.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {

    // MARK: Lets and Vars
    let monthDoc = ["01": "Jan", "02": "Feb", "03": "Mar", "04": "Apr", "05": "Mai", "06": "Jun", "07": "Jul", "08": "Aug", "09": "Sep", "10": "Okt", "11": "Nov", "12": "Dez"]
    var whiteCell: Bool?
    var owner: String?
    var messageText: String?
    var profilImage: UIImage?
    
    // MARK: - Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var profilView: UIView!
    @IBOutlet weak var profilImageView: UIImageView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var textView: UITextView!
    
    // MARK: App Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK: Functions
    func setupData(data: Message, _whiteCell: Bool) {
        whiteCell = _whiteCell
        owner = data.uid // ["klasse", "datum", "uid", "text", "name"]
        messageText = "\(data.text) \n\n-\(data.name)"
        
        let monthCode = String(data.datum.prefix(5).suffix(2))
        dateLabel.text = "\(data.datum.prefix(2)) \(monthDoc[monthCode] ?? monthCode)"
        
        setupUI()
        downloadImage()
    }
    
    func setupUI() {
        // General
        mainView.layer.cornerRadius = 25
        mainView.addPrecastShadow()
        profilView.layer.cornerRadius = 40
        profilView.addPrecastShadow()
        profilImageView.layer.cornerRadius = 40
        dateView.layer.cornerRadius = 20
        dateView.addPrecastShadow()
        borderView.layer.cornerRadius = 2
        textView.text = messageText ?? "Ein Fehler ist beim Download der Nachricht aufgetreten!"

        // Specific (Color of Message)
        mainView.backgroundColor = whiteCell! ? UIColor(named: "FirstMessageColor") : UIColor(named: "SecondMessageColor")
        borderView.backgroundColor = whiteCell! ? UIColor(white: 0.0, alpha: 1.0) : UIColor(white: 1.0, alpha: 1.0)
        dateView.backgroundColor = whiteCell! ? UIColor(red: 61/255, green: 169/255, blue: 224/255, alpha: 1) : UIColor(red: 255/255, green: 18/255, blue: 18/255, alpha: 1)
        textView.textColor = whiteCell! ? UIColor(white: 0.0, alpha: 1.0) : UIColor(red: 247/255, green: 248/255, blue: 250/255, alpha: 1)
    }
    
    // Downloads the profilimage of the owner of the message from firebase
    func downloadImage() {
        STGStorage.getImage(path: "profil_images/\(owner!).jpg", onSuccess: { (data) in
            self.profilImage = UIImage(data: data)
            self.profilImageView.image = self.profilImage
        }) { (err) in
            print("Bild der von \(self.owner!) nicht gefunden; Fehlercode: \(err.localizedDescription)")
        }
    }

}
