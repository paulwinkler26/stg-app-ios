//
//  ArticleViewController.swift
//  St. Thomas Gymnasium
//
//  Created by Paul on 04.01.20.
//  Copyright © 2020 Paul Winkler. All rights reserved.
//

import UIKit

class ArticleViewController: UIViewController {

    // MARK: Lets and Vars
    var data: News!

    // MARK: Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var backgrounImage: UIImageView!
    @IBOutlet weak var imagesView: UIView!
    @IBOutlet weak var imageTitel: UILabel!
    @IBOutlet weak var shadowImage: UIImageView!
    @IBOutlet weak var titelLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mainText: UILabel!
    @IBOutlet weak var webseiteButton: UIButton!
    @IBOutlet weak var backIcon: UIBarButtonItem!
    @IBOutlet weak var shareIcon: UIBarButtonItem!
    
    // MARK: App Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        checkImage()
        setupData()
        
        if #available(iOS 13.0, *) {} else {
            backIcon.image = UIImage(named: "backIconSmall")
            shareIcon.title = "Teilen"
        }
    }

    // MARK: Actions
    @IBAction func dismissButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func shareButtonTapped(_ sender: Any) {
        let textToShare = "Schau dir jetzt unsere Neuigkeiten in der St. Thomas Gymnasium App an!"
        
        if let urlToShare = URL(string: "http://stgapp.de/article/\(data.id)") {
            let objectsToShare = [textToShare, urlToShare] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            activityVC.popoverPresentationController?.sourceView = self.view
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func webseiteButtonTapped(_ sender: Any) {
        guard let url = URL(string: "http://www.thomas-gymnasium.de/") else { return }
        UIApplication.shared.open(url)
    }
    
    func setupData() {
        imageTitel.text = data.bildTitel
        titelLabel.text = data.titel
        nameLabel.text = "- \(data.name)"
        mainText.text = data.text
        
        title = data.titel
    }
    
    func checkImage() {
        if data.image == nil {
            downloadImage()
        } else {
            backgrounImage.image = data.image
        }
    }
    
    func downloadImage() {
        STGStorage.getImage(path: "news_images/\(data.bild).jpg", onSuccess: { (data) in
            self.data.image = UIImage(data: data)
            self.backgrounImage.image = self.data.image
        }) { (err) in
            print("Download der News ist fehlgeschlagen; ID: \(self.data.id); Fehlercode: \(err.localizedDescription)")
        }
    }
}
